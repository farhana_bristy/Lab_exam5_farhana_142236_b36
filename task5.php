<?php

$startDate = new DateTime("1981-11-03");
$endDate = new DateTime(date('Y-m-d h:i:s'));
//echo date("Y-m-d h:i:s");
// echo $endDate->format('Y/m/d');

$int = $startDate->diff($endDate);

echo $int->format('%y years, %m months, %d days, %h hours, %i minutes, %s seconds');

?>