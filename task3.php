<?php
    class MyCalculator
    {
        public $number1;
        public $number2;
        public function __construct($num1,$num2)
        {
            $this->number1 = $num1;
            $this->number2 = $num2;
        }
        public function add()
        {
            return $this->number1 + $this->number2;
        }
        public function sub()
        {
            return $this->number1 - $this->number2;
        }
        public function multiply()
        {
            return $this->number1 * $this->number2;
        }
        public function division()
        {
            return $this->number1 / $this->number2;
        }
    }

$mycalc = new MyCalculator( 12, 6);

echo 'Addition: ' . $mycalc-> add() . '<br>';

echo 'Subtraction: ' . $mycalc-> sub(). '<br>';

echo 'Multiplication: ' . $mycalc-> multiply(). '<br>';

echo  'Division: ' .$mycalc-> division(). '<br>';


?>